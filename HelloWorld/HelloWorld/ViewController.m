//
//  ViewController.m
//  HelloWorld
//
//  Created by Anthony on 1/23/17.
//  Copyright © 2017 Anthony. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)buttonPressed:(id)sender {
    //NSLog(@"Button pressed");
    //_mainTextLabel.text = @"You changed the text!";
    //[_mainTextLabel sizeToFit];
    //[_mainTextLabel setTextAlignment:NSTextAlignmentCenter];
    _mainTextLabel.text = _inputBox.text;
    
}
@end
