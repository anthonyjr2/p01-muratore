//
//  AppDelegate.h
//  HelloWorld
//
//  Created by Anthony on 1/23/17.
//  Copyright © 2017 Anthony. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

